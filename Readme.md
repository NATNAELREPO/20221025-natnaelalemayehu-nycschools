
/**
Using with MVVM arch developer this NYCSchools app to show SAT scores along with city and schools for which are located
in NYC.
Used dagger for dependency injection and for Networkig used Okhttp.
Also for caching backend responses used SQlite and using with Scheduler made it functions run in scheduled.
**/