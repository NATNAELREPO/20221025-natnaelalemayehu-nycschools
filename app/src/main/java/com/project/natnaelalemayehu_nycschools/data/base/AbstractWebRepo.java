package com.project.natnaelalemayehu_nycschools.data.base;

import com.project.natnaelalemayehu_nycschools.network.api_url_provider.ApiUrlProvider;
import com.project.natnaelalemayehu_nycschools.network.auth_token_provider.AuthTokenProvider;
import com.project.natnaelalemayehu_nycschools.network.http_client.HttpClient;

public abstract class AbstractWebRepo {

    protected final HttpClient httpClient;
    protected final AuthTokenProvider authTokenProvider;
    protected final ApiUrlProvider apiUrlProvider;

    public AbstractWebRepo(HttpClient httpClient, AuthTokenProvider authTokenProvider, ApiUrlProvider apiUrlProvider) {
        this.httpClient = httpClient;
        this.authTokenProvider = authTokenProvider;
        this.apiUrlProvider = apiUrlProvider;
    }

}
