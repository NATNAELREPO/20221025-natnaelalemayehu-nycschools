package com.project.natnaelalemayehu_nycschools.view.school_list_activity

import com.project.natnaelalemayehu_nycschools.adapters.school_list_adapter.SchoolListItemUiModel

data class SchoolListUiModel(val schoolListItemUiModels: List<SchoolListItemUiModel>, val errorMessage: String? = null)