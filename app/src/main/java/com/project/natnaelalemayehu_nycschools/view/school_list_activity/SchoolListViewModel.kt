package com.project.natnaelalemayehu_nycschools.view.school_list_activity

import androidx.lifecycle.LiveData
import com.project.natnaelalemayehu_nycschools.adapters.school_list_adapter.SchoolListItemUiModel

interface SchoolListViewModel {
    fun getSchoolList(): LiveData<SchoolListUiModel>
    fun onSchoolListItemSelected(schoolListItemUiModel: SchoolListItemUiModel)
}
