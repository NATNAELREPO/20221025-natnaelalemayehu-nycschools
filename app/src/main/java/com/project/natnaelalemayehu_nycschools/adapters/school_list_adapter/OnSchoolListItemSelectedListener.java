package com.project.natnaelalemayehu_nycschools.adapters.school_list_adapter;

public interface OnSchoolListItemSelectedListener {

    void onSchoolListItemSelected(SchoolListItemUiModel schoolListItemUiModel);

}
