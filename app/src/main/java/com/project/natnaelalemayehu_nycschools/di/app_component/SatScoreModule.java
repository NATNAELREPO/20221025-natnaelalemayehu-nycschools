package com.project.natnaelalemayehu_nycschools.di.app_component;

import com.project.natnaelalemayehu_nycschools.data.SatScoreDataDbRepoImpl;
import com.project.natnaelalemayehu_nycschools.data.SatScoreDataRepoImpl;
import com.project.natnaelalemayehu_nycschools.domain.get_sat_score_interactor.GetSatScoreDataInteractor;
import com.project.natnaelalemayehu_nycschools.domain.get_sat_score_interactor.SatScoreDataDbRepo;
import com.project.natnaelalemayehu_nycschools.domain.get_sat_score_interactor.SatScoreDataRepo;
import com.project.natnaelalemayehu_nycschools.domain.get_sat_score_interactor.impl.GetSatScoreDataInteractorImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
class SatScoreModule {

    @Singleton
    @Provides
    static GetSatScoreDataInteractor getSatScoreDataInteractor(GetSatScoreDataInteractorImpl getSatScoreDataInteractor){
        return getSatScoreDataInteractor;
    }

    @Singleton
    @Provides
    static SatScoreDataRepo satScoredDataRepo(SatScoreDataRepoImpl satScoredDataRepo){
        return satScoredDataRepo;
    }

    @Singleton
    @Provides
    static SatScoreDataDbRepo satScoredDataDbRepo(SatScoreDataDbRepoImpl satScoredDataRepo){
        return satScoredDataRepo;
    }

}
