package com.project.natnaelalemayehu_nycschools.di.app_component;

import com.project.natnaelalemayehu_nycschools.data.SchoolListDbRepoImpl;
import com.project.natnaelalemayehu_nycschools.data.SchoolListWebRepoImpl;
import com.project.natnaelalemayehu_nycschools.domain.get_school_list_interactor.GetSchoolListInteractor;
import com.project.natnaelalemayehu_nycschools.domain.get_school_list_interactor.SchoolListDbRepo;
import com.project.natnaelalemayehu_nycschools.domain.get_school_list_interactor.SchoolListRepo;
import com.project.natnaelalemayehu_nycschools.domain.get_school_list_interactor.impl.GetSchoolListInteractorImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
class SchoolListModule {

    @Singleton
    @Provides
    static SchoolListRepo getSchoolListRepo(SchoolListWebRepoImpl schoolListWebRepo){
        return schoolListWebRepo;
    }

    @Singleton
    @Provides
    static SchoolListDbRepo getSchoolListDbRepo(SchoolListDbRepoImpl schoolListDbRepo){
        return schoolListDbRepo;
    }

    @Singleton
    @Provides
    static GetSchoolListInteractor getSchoolListInteractor(GetSchoolListInteractorImpl getSchoolListInteractor){
        return getSchoolListInteractor;
    }

}
