package com.project.natnaelalemayehu_nycschools.di;

import com.project.natnaelalemayehu_nycschools.di.app_component.AppComponent;

public interface ComponentProvider {

    AppComponent getAppComponent();

}
