package com.project.natnaelalemayehu_nycschools.domain.get_school_list_interactor;

import com.project.natnaelalemayehu_nycschools.domain.get_school_list_interactor.data.SchoolListResponse;
import com.project.natnaelalemayehu_nycschools.entities.Borough;

import io.reactivex.Single;


public interface SchoolListRepo {

    Single<SchoolListResponse> getSchoolsByBorough(Borough borough);

}
