package com.project.natnaelalemayehu_nycschools.domain.get_sat_score_interactor;

import com.project.natnaelalemayehu_nycschools.entities.SatScoreData;

import io.reactivex.Completable;

public interface SatScoreDataDbRepo extends SatScoreDataRepo{

    Completable storeSatData(SatScoreData satScoreData);

}
