package com.project.natnaelalemayehu_nycschools.network.api_url_provider;

public interface ApiUrlProvider {

    String getSchoolListApi();

    String getSchoolSatApi();

}
